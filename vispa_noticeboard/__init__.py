# -*- coding: utf-8 -*-

from vispa.server import AbstractExtension
from controller import NoticeboardController

class NoticeboardExtension(AbstractExtension):

    def name(self):
        return "noticeboard"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(NoticeboardController(self))
