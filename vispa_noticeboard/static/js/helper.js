define([
  "jquery",
  "emitter"
], function (
  $,
  Emitter) {

  var NoticeboardHelper = Emitter._extend({

    init: function (NoticeboardView) {
      var self = this;

      self.nv = NoticeboardView;
    },

    getProjectByName: function (name) {
      for (var i = this.nv.vueNoticeboard.projects.length - 1; i >= 0; i--) {
        if (this.nv.vueNoticeboard.projects[i].name === name)
          return this.nv.vueNoticeboard.projects[i];
      }
      return null;
    },

    getCurrentProject: function () {
      return this.getProjectByName(this.nv.state.get("project"));
    },

  });

  return NoticeboardHelper;
});