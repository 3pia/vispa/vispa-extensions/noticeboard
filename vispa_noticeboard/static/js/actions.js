define([
  "jquery",
  "emitter",
  "text!../html/dialog.html"
], function (
  $,
  Emitter,
  dialogTemplate) {

  var NoticeboardActions = Emitter._extend({

    init: function (NoticeboardView) {
      var self = this;

      self.nv = NoticeboardView;
    },

    newNote: function () {
      var self = this;

      var dialog = null;
      var $body = $(dialogTemplate);

      self.nv.dialog({
        title: "Write new note on noticeboard.",
        body: $body.html(),
        buttons: {
          cancel: {
            label: "cancel",
            iconClass: "fa-remove",
            class: "btn-danger",
            pos: 0
          },
          ok: {
            label: "ok",
            iconClass: "fa-check",
            class: "btn-success",
            pos: 1,
            callback: function () {
              var title = $("#title", this.$el)[0].value;
              var content = $("#content", this.$el)[0].value;
              if (title !== "") {
                notecontent = {
                  title: title,
                  content: content,
                  date: new Date().toString().substr(0, 21),
                  author: vispa.args.user.name
                };
                self.nv.POST("create_note", {
                  project: self.nv.state.get("project"),
                  content: JSON.stringify(notecontent)
                }, function () {
                  self.nv.loadContent(self.nv.helper.getCurrentProject());
                });
              } else {
                self.nv.alert("Please provide a title.");
                return true;
              }
            }
          }
        },
        onOpen: function () {
          vispa.nextTick(function (test) {
            $("#title", this.$el)[0].focus();
          }.bind(this));
        },
      });
    },

    editNote: function (note) {
      var self = this;

      var dialog = null;
      var $body = $(dialogTemplate);

      self.nv.dialog({
        title: "Write new note on noticeboard.",
        body: $body.html(),
        buttons: {
          cancel: {
            label: "cancel",
            iconClass: "fa-remove",
            class: "btn-danger",
            pos: 0
          },
          ok: {
            label: "ok",
            iconClass: "fa-check",
            class: "btn-success",
            pos: 1,
            callback: function () {
              var title = $("#title", this.$el)[0].value;
              var content = $("#content", this.$el)[0].value;
              if (title !== "") {
                self.nv.POST("edit_note", {
                  id: note.id,
                  newtitle: title,
                  newcontent: content
                }, function () {
                  self.nv.loadContent(self.nv.helper.getCurrentProject());
                });
              } else {
                self.nv.alert("Please provide a title.");
                return true;
              }
            }
          }
        },
        onOpen: function () {
          vispa.nextTick(function (test) {
            $("#title", this.$el).val(note.title);
            $("#content", this.$el).val(note.content);
            $("#title", this.$el)[0].focus();
          }.bind(this));
        },
      });
    },

    deleteNote: function (note) {
      var self = this;

      self.nv.confirm(
        "Do you want to delete the note \"" + note.title + "\"",
        function (res) {
          if (res) {
            self.nv.POST("delete_item", {
              id: note.id
            }, function () {
              self.nv.loadContent(self.nv.helper.getCurrentProject());
            });
          }
        }
      );
    },

    newFile: function () {
      var self = this;

      self.nv.spawnInstance("file", "FileSelector", {
        callback: function (path) {
          if (!path) return;
          file = {
            name: path.substring(path.lastIndexOf("/") + 1, path.length),
            workspaceID: self.nv.workspace.id,
            path: path.substring(0, path.lastIndexOf("/") + 1),
            date: new Date().toString().substr(0, 21),
            author: vispa.args.user.name
          }
          self.nv.POST("create_file", {
            project: self.nv.state.get("project"),
            content: JSON.stringify(file)
          }, function () {
            self.nv.loadContent(self.nv.helper.getCurrentProject());
          });
          self.nv.alert("The ACL permissions are not set automatically. Please do this manuelly.");
        }
      });
    },

    showFile: function (file) {
      if (~$.inArray(file.name.split(".").pop().toLowerCase(), this.nv.showExtensions)) {
        path = file.path + file.name
        vispa.fileHandler.runDefaultHandler(path, this.nv.instance);
      } else {
        this.nv.error("File can not be shown.")
      }
    },

    copyFile: function (file) {
      this.nv.POST("/ajax/fs/paste", {
        path: this.nv.prefs.get("copyPath"),
        paths: JSON.stringify([file.path + file.name]),
        cut: false
      }, function (error) {
        if (error) {
          this.nv.alert(error.responseText);
        } else {
          vispa.messenger.info("Copying successfull.");
        }
      }.bind(this));
    },

    deleteFile: function (file) {
      var self = this;

      self.nv.confirm(
        "Do you want to delete the file \"" + file.name + "\"",
        function (res) {
          if (res) {
            self.nv.POST("delete_item", {
              id: file.id
            }, function () {
              self.nv.loadContent(self.nv.helper.getCurrentProject());
            });
          }
        }
      );
    },

  });

  return NoticeboardActions;
});