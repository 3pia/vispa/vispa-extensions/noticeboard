// noticeboard extension
define([
  "vispa/extension",
  "./view",
  "css!../css/noticeboard.css"
], function (
  Extension,
  NoticeboardView) {

  var NoticeboardExtension = Extension._extend({

    init: function init() {
      init._super.call(this, "noticeboard", "Noticeboard");

      this.mainMenuAdd([this.addView(NoticeboardView)]);
    }
  });

  return NoticeboardExtension;
});