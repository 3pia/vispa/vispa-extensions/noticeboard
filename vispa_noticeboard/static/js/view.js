define([
  "vispa/views/main",
  "jquery",
  "vue/vue",
  "./actions",
  "./helper",
  "text!../html/noticeboard.html"
], function (
  MainView,
  $,
  Vue,
  NoticeboardActions,
  NoticeboardHelper,
  Tnoticeboard) {

  var Noticeboard = Vue.extend({
    template: Tnoticeboard.trim(),
    data: function () {
      return {
        projects: {},
        currentProject: null,
      };
    },
    computed: {
      buttonClass: function () {
        if (this.currentProject && this.currentProject.canCreate) return "btn btn-default";
        else return "btn btn-default disabled";
      }
    },
    methods: {
      selectProject: function (project) {},
      toggleNote: function (note) {},
      newNote: function () {},
      editNote: function (note) {},
      deleteNote: function (note) {},
      newFile: function () {},
      showFile: function (file) {},
      copyFile: function (file) {},
      deleteFile: function (file) {}
    }
  });

  var NoticeboardView = MainView._extend({

    init: function init(args) {
      init._super.apply(this, arguments);
      var self = this;

      self.actions = new NoticeboardActions(self);
      self.helper = new NoticeboardHelper(self);

      self.showExtensions = ["png", "jpg", "jpeg", "bmp", "pdf"];
      self.permissions = {};
      self.projects = undefined;
      self.$parentnode = undefined;

      // set default state
      this.state.setup({
        project: null,
        expandedNoteIds: []
      }, args);
    },

    getFragment: function () {
      return this.state.get("project");
    },

    applyFragment: function (fragment) {
      this.state.set("project", fragment);
      return this;
    },

    onFocus: function () {
      this.getProjects();
    },

    render: function (node) {
      var self = this;

      self.$parentnode = $(node);
      self.$parentnode.addClass("noticeboard");

      self.vueNoticeboard = new Noticeboard({
        el: node[0],
        data: {},
        methods: {
          selectProject: function (project) {
            this.currentProject = project;
            self.state.set("project", project.name)
          },
          toggleNote: function (note) {
            note.expanded = !note.expanded;
            if (note.expanded) {
              ids = self.state.get("expandedNoteIds");
              ids.push(note.id);
              self.state.set("expandedNoteIds", ids);
            } else {
              ids = self.state.get("expandedNoteIds");
              ids.splice(ids.indexOf(note.id), 1)
              self.state.set("expandedNoteIds", ids);
            }
          },
          newNote: function () {
            if (this.currentProject.canCreate) self.actions.newNote();
          },
          editNote: function (note) {
            self.actions.editNote(note);
          },
          deleteNote: function (note) {
            self.actions.deleteNote(note);
          },
          newFile: function () {
            if (this.currentProject.canCreate) self.actions.newFile();
          },
          showFile: function (file) {
            self.actions.showFile(file);
          },
          copyFile: function (file) {
            self.actions.copyFile(file);
          },
          deleteFile: function (file) {
            self.actions.deleteFile(file);
          }
        },
        watch: {
          "currentProject": function (project) {
            self.state.set("project", project.name);
            self.loadContent(project)
          }
        },
      });

      self.getProjects();
    },

    getProjects: function () {
      this.GET("/ajax/um/user_get_projects", {}, function (err, res) {
        if (!err) {
          this.vueNoticeboard.$set("projects", res);
          if (this.state.get("project")) {
            for (var i = this.vueNoticeboard.projects.length - 1; i >= 0; i--) {
              if (this.vueNoticeboard.projects[i].name === this.state.get("project")) {
                this.vueNoticeboard.currentProject = this.vueNoticeboard.projects[i];
                break;
              }
            }
          }
        }
      }.bind(this));
    },

    loadContent: function (project) {
      name = $.type(project) === "string" ? project : project.name
        // load items
      this.setLoading(true);
      this.GET("get_items", {
        project: name
      }, function (err, res) {
        if (!err) {
          for (var i = res.notes.length - 1; i >= 0; i--) {
            res.notes[i].expanded = Boolean(~$.inArray(res.notes[i].id,
              this.state.get("expandedNoteIds")));
          }
          for (var i = res.files.length - 1; i >= 0; i--) {
            res.files[i].showable = Boolean(~$.inArray(res.files[i].name.split(".").pop(),
              this.showExtensions));
          }
          Vue.set(project, "notes", res.notes);
          Vue.set(project, "files", res.files);
          this.setLoading(false);
        }
      }.bind(this));
      // load perissions for button
      this.setLoading(true);
      this.GET("can_create", {
        project: name
      }, function (err, res) {
        if (!err) {
          Vue.set(project, "canCreate", res);
          this.setLoading(false);
        }
      }.bind(this));
    }

  }, {
    iconClass: "glyphicon-pushpin",
    label: "Noticeboard",
    name: "NoticeboardView",
    menuPosition: 100,
    preferences: {
      items: {
        copyPath: {
          type: "string",
          value: "$HOME",
          description: "The noticeboard copies files per default into this path."
        }
      }
    },
    fileHandlers: {},
  });

  return NoticeboardView;
});