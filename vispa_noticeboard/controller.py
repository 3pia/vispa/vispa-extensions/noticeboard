# -*- coding: utf-8 -*-

# imports
import cherrypy
import json
from vispa import AjaxException
from vispa.controller import AbstractController
from vispa.models.project import Project, ProjectItem
from vispa.models.role import Permission

class NoticeboardController(AbstractController):

    def __init__(self, extension):
        AbstractController.__init__(self)
        self.extension = extension

    @cherrypy.expose
    def get_items(self, project):
        db = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(db, project)

        read_permission = Permission.get(db, "noticeboard.read_items")
        edit_permission = Permission.get(db, "noticeboard.edit_items")
        delete_permission = Permission.get(db, "noticeboard.delete_items")

        data = {
            "notes": [],
            "files": []
        }

        if user.has_permission(read_permission, project):
            notes = project.get_items("noticeboard.note")
            for note in notes:
                content = json.loads(note.content)
                data["notes"].append({
                    "id"       : note.id,
                    "editable" : user.has_permission(edit_permission, project)\
                                or content["author"] == user.name,
                    "deletable": user.has_permission(delete_permission, project)\
                                or content["author"] == user.name,
                    "title"    : content["title"],
                    "content"  : content["content"],
                    "author"   : content["author"],
                    "date"     : content["date"]
                })
            files = project.get_items("noticeboard.file")
            for file in files:
                content = json.loads(file.content)
                data["files"].append({
                    "id"         : file.id,
                    "deletable"  : user.has_permission(delete_permission, project)\
                                   or content["author"] == user.name,
                    "name"       : content["name"],
                    "workspaceID": content["workspaceID"],
                    "path"       : content["path"],
                    "author"     : content["author"],
                    "date"       : content["date"]
                })
            return data

        else:
            return {}

    @cherrypy.expose
    def can_create(self, project):
        db = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(db, project)

        permission = Permission.get(db, "noticeboard.create_items")

        return user.has_permission(permission, project)

    @cherrypy.expose
    def delete_item(self, id):
        db = cherrypy.request.db
        user = cherrypy.request.user
        item = ProjectItem.get(db, id)
        content = json.loads(item.content)
        project = Project.get_by_id(db, item.project_id)
        permission = Permission.get(db, "noticeboard.delete_items")

        if (user.has_permission(permission, project) or content["author"] == user.name):
            item.delete(db)
        else:
            raise AjaxException(403)

    @cherrypy.expose
    def create_note(self, project, content):
        db = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(db, project)

        permission = Permission.get(db, "noticeboard.create_items")

        if user.has_permission(permission, project):
            ProjectItem.create(db, project, "noticeboard.note", content)
        else:
            raise AjaxException(403)

    @cherrypy.expose
    def edit_note(self, id, newtitle, newcontent):
        db = cherrypy.request.db
        user = cherrypy.request.user
        item = ProjectItem.get(db, id)
        content = json.loads(item.content)
        project = Project.get_by_id(db, item.project_id)
        permission = Permission.get(db, "noticeboard.edit_items")

        if user.has_permission(permission, project) or content["author"] == user.name:
            content["title"] = newtitle
            content["content"] = newcontent
            item.set_content(json.dumps(content))
        else:
            raise AjaxException(403)

    @cherrypy.expose
    def create_file(self, project, content):
        db = cherrypy.request.db
        user = cherrypy.request.user
        project = Project.get(db, project)

        permission = Permission.get(db, "noticeboard.create_items")

        if user.has_permission(permission, project):
            ProjectItem.create(db, project, "noticeboard.file", content)
        else:
            raise AjaxException(403)
