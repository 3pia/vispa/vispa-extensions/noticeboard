#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name             = "vispa_noticeboard",
    version          = "0.0.0",
    description      = "VISPA Noticeboard - Post notes and share files in a project.",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    packages         = ["vispa_noticeboard"],
    package_dir      = {"vispa_noticeboard": "vispa_noticeboard"},
    package_data     = {"vispa_noticeboard": [
        "static/css/*",
        "static/html/*",
        "static/js/*",
    ]},
    # install_requires = ["vispa"],
)
